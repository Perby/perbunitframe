-----------------------------------------------------------------------------------------------
-- Client Lua Script for PerbUnitFrame
-- Copyright (c) NCsoft. All rights reserved
-----------------------------------------------------------------------------------------------
 
require "Window"
require "Unit"
require "GameLib"
require "Apollo"
require "PathMission"
require "P2PTrading" 

-----------------------------------------------------------------------------------------------
-- PerbUnitFrame Module Definition
-----------------------------------------------------------------------------------------------
local PerbUnitFrame = {} 

-- local tTargetMarkSpriteMap = {
  -- "Icon_Windows_UI_CRB_Marker_Bomb",
  -- "Icon_Windows_UI_CRB_Marker_Ghost",
  -- "Icon_Windows_UI_CRB_Marker_Mask",
  -- "Icon_Windows_UI_CRB_Marker_Octopus",
  -- "Icon_Windows_UI_CRB_Marker_Pig",
  -- "Icon_Windows_UI_CRB_Marker_Chicken",
  -- "Icon_Windows_UI_CRB_Marker_Toaster",
  -- "Icon_Windows_UI_CRB_Marker_UFO"
-- }

-- local tClassName = {
  -- [GameLib.CodeEnumClass.Warrior]      = "Warrior",
  -- [GameLib.CodeEnumClass.Engineer]     = "Engineer",
  -- [GameLib.CodeEnumClass.Esper]        = "Esper",
  -- [GameLib.CodeEnumClass.Medic]        = "Medic",
  -- [GameLib.CodeEnumClass.Stalker]      = "Stalker",
  -- [GameLib.CodeEnumClass.Spellslinger] = "Spellslinger"
-- }

local dispositionBarColors =
{
	[Unit.CodeEnumDisposition.Neutral]  = ApolloColor.new("xkcdPumpkinOrange"),
	[Unit.CodeEnumDisposition.Hostile]  = ApolloColor.new("xkcdLipstickRed"),
	[Unit.CodeEnumDisposition.Friendly] = ApolloColor.new("xkcdBoringGreen"),
}

local dispositionTextColors =
{
	[Unit.CodeEnumDisposition.Neutral]  = ApolloColor.new("xkcdAmber"),
	[Unit.CodeEnumDisposition.Hostile]  = ApolloColor.new("xkcdLightRed"),
	[Unit.CodeEnumDisposition.Friendly] = ApolloColor.new("xkcdBabyGreen"),
}
 
-----------------------------------------------------------------------------------------------
-- Initialization
-----------------------------------------------------------------------------------------------
function PerbUnitFrame:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function PerbUnitFrame:Init()
    Apollo.RegisterAddon(self)
end

-----------------------------------------------------------------------------------------------
-- PerbUnitFrame OnLoad
-----------------------------------------------------------------------------------------------
function PerbUnitFrame:OnLoad()
    -- load our form file
	self.xmlDoc = XmlDoc.CreateFromFile("PerbUnitFrame.xml")
	self.xmlDoc:RegisterCallback("OnDocLoaded", self)
end

-----------------------------------------------------------------------------------------------
-- PerbUnitFrame OnDocLoaded
-----------------------------------------------------------------------------------------------
function PerbUnitFrame:OnDocLoaded()
	if  self.xmlDoc == nil then	return end		
	
	Apollo.LoadSprites("BorderSprite.xml", "Perb")
	
	Apollo.RegisterSlashCommand("focus"						 , "OnFocusSlashCommand"		  , self)
	Apollo.RegisterEventHandler("WindowManagementReady"      , "OnWindowManagementReady"      , self)
	Apollo.RegisterEventHandler("WindowManagementUpdate"	 , "OnWindowManagementUpdate"	  , self)
	Apollo.RegisterEventHandler("TargetUnitChanged"          , "OnTargetUnitChanged"          , self)
	Apollo.RegisterEventHandler("AlternateTargetUnitChanged" , "OnFocusUnitChanged"           , self)
	Apollo.RegisterEventHandler("PlayerLevelChange"          , "OnUnitLevelChange"            , self)
	Apollo.RegisterEventHandler("UnitLevelChanged"           , "OnUnitLevelChange"            , self)
	Apollo.RegisterEventHandler("VarChange_FrameCount"       , "OnFrame"                      , self)
	
	self.wndPlayer = Apollo.LoadForm(self.xmlDoc, "PlayerFrame", "FixedHudStratum", self)
	self.wndTarget = Apollo.LoadForm(self.xmlDoc, "TargetFrame", "FixedHudStratum", self)
	self.wndToT = Apollo.LoadForm(self.xmlDoc, "ToTFrame", "FixedHudStratum", self)
	self.wndFocus = Apollo.LoadForm(self.xmlDoc, "FocusFrame", "FixedHudStratum", self)
	
	self.wndPlayerBuffs = Apollo.LoadForm(self.xmlDoc, "pBuffs", "FixedHudStratum", self)
	
	self.wndTarget:Show(false, true)
	self.wndToT:Show(false, true)
	self.wndFocus:Show(false, true)
	
	self.tFrameLeft, self.tFrameTop, self.tFrameRight, self.tFrameBottom = self.wndTarget:FindChild("Healthbar"):GetAnchorOffsets()
	self.totFrameLeft, self.totFrameTop, self.totFrameRight, self.totFrameBottom = self.wndToT:FindChild("Healthbar"):GetAnchorOffsets()
	self.fFrameLeft, self.fFrameTop, self.fFrameRight, self.fFrameBottom = self.wndFocus:FindChild("Healthbar"):GetAnchorOffsets()

	self.playerBeneficial = PufLibs.BuffManager:new(self.xmlDoc, "PlayerBuffs", nil, 9)
	self.playerHarmful = PufLibs.BuffManager:new(self.xmlDoc, "PlayerFrame:PlayerDebuffs", self.wndPlayer, 9)
	self.targetBeneficial = PufLibs.BuffManager:new(self.xmlDoc, "TargetFrame:TargetBuffs", self.wndTarget, 9)
end

-----------------------------------------------------------------------------------------------
-- PerbUnitFrame Functions
-----------------------------------------------------------------------------------------------
function PerbUnitFrame:OnWindowManagementReady()
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndPlayer, strName = "PerbPlayerFrame"})
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndTarget, strName = "PerbTargetFrame"})
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndToT, strName = "PerbToTFrame"})
	Event_FireGenericEvent("WindowManagementAdd", {wnd = self.wndFocus, strName = "PerbFocusFrame"})
end

function PerbUnitFrame:OnWindowManagementUpdate(frame)
	if frame and frame.wnd and (frame.wnd == self.wndToT or frame.wnd == self.wndTarget or frame.wnd == self.wndPlayer or frame.wnd == self.wndFocus) then 
		local bMoveable = frame.wnd:IsStyleOn("Moveable")

--make frames resizable eventually
		-- frame.wnd:SetStyle("Sizable", bMoveable)
		frame.wnd:SetStyle("RequireMetaKeyToMove", bMoveable)
		-- frame.wnd:SetStyle("IgnoreMouse", not bMoveable)
	end
end

function PerbUnitFrame:OnFrame()
	local unitPlayer = GameLib.GetPlayerUnit()
	local nHealthMax = unitPlayer:GetMaxHealth()
	local nHealthCurrent = unitPlayer:GetHealth()
	local nShieldMax = unitPlayer:GetShieldCapacityMax()
	local nShieldCurrent = unitPlayer:GetShieldCapacity()
	local CCArmorValue = unitPlayer:GetInterruptArmorValue()
	local CCArmorMax = unitPlayer:GetInterruptArmorMax()
	
	self.wndPlayer:SetData(GameLib.GetPlayerUnit())
	
	self.wndPlayer:FindChild("Healthbar"):SetMax(nHealthMax)
	self.wndPlayer:FindChild("Healthbar"):SetProgress(nHealthCurrent)
	self.wndPlayer:FindChild("HealthText"):SetText(self:siValue(nHealthCurrent))
	
	self.wndPlayer:FindChild("Shieldbar"):SetMax(nShieldMax)
	self.wndPlayer:FindChild("Shieldbar"):SetProgress(nShieldCurrent)
	self.wndPlayer:FindChild("ShieldText"):SetText(self:siValue(nShieldCurrent))
  
	if CCArmorMax == 0 or CCArmorValue == nil then
		self.wndPlayer:FindChild("IA"):SetText("")
	else
		if CCArmorMax == -1 then --infinite
			self.wndPlayer:FindChild("IA"):SetText("/~/")
		-- elseif CCArmorValue == 0 and nCCArmorMax > 0 then --broken
			-- self.wndPlayer:FindChild("IA"):SetText(CCArmorValue)
		elseif CCArmorMax > 0 then -- has armor, has value
			self.wndPlayer:FindChild("IA"):SetText(CCArmorValue)
		end
	end
	
	local unitTarget = unitPlayer:GetTarget()
	self:UpdateTargetFrame(unitTarget)
	
	local unitFocus = unitPlayer:GetAlternateTarget()
	self:UpdateFocusFrame(unitFocus)
	
	-- self:BuffThings()
	self:UpdateBuffs()
end

function PerbUnitFrame:UpdateBuffs()
	local unitPlayer = GameLib.GetPlayerUnit()
	local unitTarget = unitPlayer:GetTarget()
	local playerBuffs = unitPlayer:GetBuffs()
	local targetBuffs = unitTarget ~= nil and unitTarget:GetBuffs() or {arBeneficial={}, arHarmful={}}

	self.playerBeneficial:Update(playerBuffs.arBeneficial)
	self.playerHarmful:Update(playerBuffs.arHarmful)
	self.targetBeneficial:Update(targetBuffs.arBeneficial)
end

function PerbUnitFrame:OnTargetUnitChanged(unitTarget)
	self:UpdateTargetFrame(unitTarget)
	self:UpdateToTFrame(unitToT)
end

function PerbUnitFrame:UpdateTargetFrame(unitTarget)
	if unitTarget == nil then self.wndTarget:Show(false, true) return end
	
	local unitPlayer = GameLib.GetPlayerUnit()
	local eDisposition = unitTarget:GetDispositionTo(unitPlayer)
	local name = unitTarget:GetName()
	local unitTarget = unitPlayer:GetTarget()
	local nHealthMax = unitTarget:GetMaxHealth()
	local nHealthCurrent = unitTarget:GetHealth()
	local nShieldMax = unitTarget:GetShieldCapacityMax()
	local nShieldCurrent = unitTarget:GetShieldCapacity()
	local CCArmorValue = unitTarget:GetInterruptArmorValue()
	local CCArmorMax = unitTarget:GetInterruptArmorMax()
	local nHealthPercent = 100
	
	self.wndTarget:SetData(unitTarget)

	self.wndTarget:FindChild("NameText"):SetText(name)
	self.wndTarget:FindChild("NameText"):SetTextColor(dispositionTextColors[eDisposition])
	
	self.wndTarget:FindChild("Healthbar"):SetMax(nHealthMax)
	self.wndTarget:FindChild("Healthbar"):SetProgress(nHealthCurrent)
	self.wndTarget:FindChild("Healthbar"):SetBarColor(dispositionBarColors[eDisposition])
	
	if nHealthCurrent ~=nil then
		nHealthPercent = math.floor(nHealthCurrent / nHealthMax * 100)
		self.wndTarget:FindChild("HealthText"):SetText(self:siValue(nHealthCurrent) .. "   " .. nHealthPercent .. "%")
		self.wndTarget:FindChild("HealthText"):SetTextColor(dispositionTextColors[eDisposition])
	else
		self.wndTarget:FindChild("HealthText"):SetText("")
	end
	
	self.wndTarget:FindChild("Shieldbar"):SetMax(nShieldMax)
	self.wndTarget:FindChild("Shieldbar"):SetProgress(nShieldCurrent)	
	
	if nShieldMax == 0 then
		self.wndTarget:FindChild("Healthbar"):SetAnchorOffsets(self.tFrameLeft, self.tFrameTop, self.tFrameRight+68, self.tFrameBottom)
		self.wndTarget:FindChild("ShieldText"):SetText("")
	else
		self.wndTarget:FindChild("Healthbar"):SetAnchorOffsets(self.tFrameLeft, self.tFrameTop, self.tFrameRight, self.tFrameBottom)
		self.wndTarget:FindChild("ShieldText"):SetText(self:siValue(nShieldCurrent))
	end
  
	if CCArmorMax == 0 or CCArmorValue == nil then
		self.wndTarget:FindChild("IA"):SetText("")
	else
		if CCArmorMax == -1 then --infinite
			self.wndTarget:FindChild("IA"):SetText("/~/")
		elseif CCArmorMax > 0 then -- has armor, has value
			self.wndTarget:FindChild("IA"):SetText(CCArmorValue)
		end
	end
	
	self.wndTarget:Show(true, true)
	
	local unitToT = unitTarget:GetTarget()
	self:UpdateToTFrame(unitToT)
end

function PerbUnitFrame:UpdateToTFrame(unitToT)
	if unitToT == nil then self.wndToT:Show(false, true) return end
	
	local unitPlayer = GameLib.GetPlayerUnit()
	local unitTarget = unitPlayer:GetTarget()
	local unitToT = unitTarget:GetTarget()
	
	local eDisposition = unitToT:GetDispositionTo(unitPlayer)
	local name = unitToT:GetName()
	local healthCurrent = unitToT:GetHealth()
	local healthMax = unitToT:GetMaxHealth()
	local nShieldMax = unitToT:GetShieldCapacityMax()
	local nShieldCurrent = unitToT:GetShieldCapacity()
	
	self.wndToT:SetData(unitToT)
	
	self.wndToT:FindChild("Healthbar"):SetMax(healthMax)
	self.wndToT:FindChild("Healthbar"):SetProgress(healthCurrent)
	self.wndToT:FindChild("Healthbar"):SetBarColor(dispositionBarColors[eDisposition])
	
	self.wndToT:FindChild("NameText"):SetText(name)
	self.wndToT:FindChild("NameText"):SetTextColor(dispositionTextColors[eDisposition])
	
	self.wndToT:FindChild("Shieldbar"):SetMax(nShieldMax)
	self.wndToT:FindChild("Shieldbar"):SetProgress(nShieldCurrent)	
	
	if nShieldMax == 0 then
		self.wndToT:FindChild("Healthbar"):SetAnchorOffsets(self.totFrameLeft, self.totFrameTop, self.totFrameRight+68, self.totFrameBottom)
	else
		self.wndToT:FindChild("Healthbar"):SetAnchorOffsets(self.totFrameLeft, self.totFrameTop, self.totFrameRight, self.totFrameBottom)
	end
	
	self.wndToT:Show(true, true)
end

function PerbUnitFrame:OnAlternateTargetChanged(unitFocus)
	self:UpdateFocusFrame(unitFocus)
end

function PerbUnitFrame:OnFocusSlashCommand()
	local unitTarget = GameLib.GetPlayerUnit():GetTarget()
	GameLib.GetPlayerUnit():SetAlternateTarget(unitTarget)
end

function PerbUnitFrame:UpdateFocusFrame(unitFocus)
	if unitFocus == nil then self.wndFocus:Show(false, true) return end
	
	local unitPlayer = GameLib.GetPlayerUnit()
	local unitFocus = unitPlayer:GetAlternateTarget()
	
	local eDisposition = unitFocus:GetDispositionTo(unitPlayer)
	local name = unitFocus:GetName()
	local healthCurrent = unitFocus:GetHealth()
	local healthMax = unitFocus:GetMaxHealth()
	local nShieldMax = unitFocus:GetShieldCapacityMax()
	local nShieldCurrent = unitFocus:GetShieldCapacity()
	
	self.wndFocus:SetData(unitFocus)
	
	self.wndFocus:FindChild("Healthbar"):SetMax(healthMax)
	self.wndFocus:FindChild("Healthbar"):SetProgress(healthCurrent)
	self.wndFocus:FindChild("Healthbar"):SetBarColor(dispositionBarColors[eDisposition])
	
	self.wndFocus:FindChild("NameText"):SetText(name)
	self.wndFocus:FindChild("NameText"):SetTextColor(dispositionTextColors[eDisposition])
	
	self.wndFocus:FindChild("Shieldbar"):SetMax(nShieldMax)
	self.wndFocus:FindChild("Shieldbar"):SetProgress(nShieldCurrent)	
	
	if nShieldMax == 0 then
		self.wndFocus:FindChild("Healthbar"):SetAnchorOffsets(self.fFrameLeft, self.fFrameTop, self.fFrameRight+68, self.fFrameBottom)
	else
		self.wndFocus:FindChild("Healthbar"):SetAnchorOffsets(self.fFrameLeft, self.fFrameTop, self.fFrameRight, self.fFrameBottom)
	end	

	self.wndFocus:Show(true, true)
end

function PerbUnitFrame:OnMouseButtonDown(wndHandler, wndControl, eMouseButton, x, y)
	if wndHandler ~= wndControl then return end --this fixes buffs having a right click menu
	
	local unitRightClick = wndHandler:GetData()
	
	if eMouseButton == GameLib.CodeEnumInputMouse.Left and unitRightClick ~= nil then
		GameLib.SetTargetUnit(unitRightClick)
		return false
	end
	
	if eMouseButton == GameLib.CodeEnumInputMouse.Right and unitRightClick ~= nil then
		Event_FireGenericEvent("GenericEvent_NewContextMenuPlayerDetailed", nil, unitRightClick:GetName(), unitRightClick)
		return true
	end
	
	return false
end

function PerbUnitFrame:siValue(val)
	if val == nil then return end
	if(val>999999) then
		return ("%.1fm"):format(val * 0.000001)
	elseif(val>999) then
		return ("%.1fk"):format(val * 0.001) 
	else
		return val
	end
end

local PerbUnitFrameInst = PerbUnitFrame:new()
PerbUnitFrameInst:Init()