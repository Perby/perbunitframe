local BuffManager = {}
-- BuffManager.__index = BuffManager

function BuffManager:OnGenerateBuffTooltip(wndHandler, wndControl, tType, splBuff)
	if wndHandler == wndControl then
		return
	end

	Tooltip.GetSpellTooltipForm(self, wndHandler, wndHandler:GetData(), false)
end

function BuffManager:new(xmlDoc, templateName, parent, framesPerRow, padding, frameCount)
    local o = {}
    setmetatable(o, self)
    self.__index = self 

    o.parent = parent
    o.framesPerRow = framesPerRow or 999
	o.padding = padding or 1 -- default value
	o.xmlDoc = xmlDoc
	o.templateName = templateName
	o.baseFrame = Apollo.LoadForm(xmlDoc, templateName, parent, self)
	o.buffs = {}
	o.nextPosition = 1

	o.baseFrame:Show(false)

	-- frame pool (create 10 frames by default)
	o.frameCount = 0
	o:CreateFramePool(frameCount or 10)

	return o
end

function BuffManager:CreateFramePool(count)
	for i=self.frameCount+1, count do
		local buff = {
			frame = Apollo.LoadForm(self.xmlDoc, self.templateName, self.parent, self)
		}

		buff.frame:Show(false)
		self:SetAnchors(buff, i)

		-- store the empty frame
		self.buffs[i] = buff
	end

	-- store the number of frames we have
	self.frameCount = count
end

-- position in the list; starts at 1
function BuffManager:SetAnchors(buff, position)
	position = position - 1 -- calculate position from 0, rather than 1

	local leftAnchor, topAnchor, rightAnchor, bottomAnchor = self.baseFrame:GetAnchorOffsets()
	local width = self.baseFrame:GetWidth()
	local height = self.baseFrame:GetHeight()

	local hOffset = (position % self.framesPerRow) * (width + self.padding)
	local vOffset = 0

	if position >= self.framesPerRow then
		vOffset = math.floor(position / self.framesPerRow) * (height + self.padding)
	end

	buff.frame:SetAnchorOffsets(leftAnchor + hOffset, topAnchor + vOffset, rightAnchor + hOffset, bottomAnchor + vOffset)
end

function BuffManager:Update(buffList)
	self:ResetPosition()

	local i = #buffList

	-- create new frames if we need to
	if i > self.frameCount then
		self:CreateFramePool(i)
	end

	-- hide all the frames after the last one we need
	for t=1, self.frameCount do
		self.buffs[t].frame:Show(false)
	end

	for _, data in pairs(buffList) do
		local buff = self.buffs[i]
		local spell = data.splEffect
		local duration = data.fTimeRemaining
		local count = data.nCount
		
		buff.frame:SetData(spell)

		buff.frame:FindChild("Icon"):SetSprite(spell:GetIcon())
		buff.frame:FindChild("Cooldown"):SetText(self:FormatDuration(duration))
		buff.frame:FindChild("Count"):SetText(count > 1 and count or "")

		buff.frame:Show(true)

		i = i-1
	end
end

function BuffManager:ResetPosition()
	self.nextPosition = 1
end

-- returns the current position and increments the position
function BuffManager:NextPosition()
	local currentPosition = self.nextPosition
	self.nextPosition = self.nextPosition + 1
	return currentPosition
end

function BuffManager:FormatDuration(tim)
	if tim == nil then return end 
	if (tim>86400) then
		return ("%.0fd"):format(tim/86400)
	elseif (tim>3600) then
		return ("%.0fh"):format(tim/3600)
	elseif (tim>60) then
		return ("%.0fm"):format(tim/60)
	elseif (tim>5) then
		return ("%.0fs"):format(tim)
	elseif (tim>0) then
		return ("%.1fs"):format(tim)
	elseif (tim==0) then
		return ""
	end
end

-- PerbUnitFrame lib
if _G["PufLibs"] == nil then _G["PufLibs"] = {} end
_G["PufLibs"]["BuffManager"] = BuffManager